# **UiPath Small Starter Pack**

Don't have an idea for automation? Small starter pack will help you to learn  and familiar with UiPath. We provide UiPath Starter Pack to let you  experience the look and feel of UIPath and trial it yourself. We offer  support for you to plan, build, manage, run, and measure automation  using UiPath.

You can have update about this tutorial by follow our website or social media.

- **Website:** [UIPath ISID](https://www.isid.co.id/uipath)
- **FB:** [@IndonesiaIsid](https://www.facebook.com/indonesiaisid), 
- **Twitter:** [@indonesiaisid](https://twitter.com/IndonesiaIsid) 
- **Instagram:**  [@isid.indonesia](https://www.instagram.com/isid.indonesia/) 
- **Youtube:**  [Isid Indonesia](https://www.youtube.com/channel/UC93Te2tm_B6vynFADbVuKjw)



## **Check our UiPath Starter pack series**

1.  [Input Bank Statement Automation](https://www.isid.co.id/uipath-inputbankstatement)
2. [Collect Invoice from Emails Automation](https://www.isid.co.id/uipath-collect-invoice-from-email)
3. [CSV to Excel Automation](https://www.isid.co.id/uipath-csv-to-excel-automation)
4. [Split Excel Sheet into Multiple Sheets Automation](https://www.isid.co.id/uipath-split-excel-sheet-automation)
5. [PDF table to Excel Automation](https://www.isid.co.id/uipath-pdf-excel-automation)
6. [Collect Customers Data from Website Automation](https://www.isid.co.id/uipath-get-data-customer-automation)
7. Generate Excel Report from Database Automation

