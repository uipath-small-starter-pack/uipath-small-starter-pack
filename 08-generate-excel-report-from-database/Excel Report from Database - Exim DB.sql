USE [master]
GO
/****** Object:  Database [Exim]    Script Date: 1/21/2021 2:31:50 PM ******/
CREATE DATABASE [Exim]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Exim', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\Exim.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Exim_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\Exim_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [Exim] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Exim].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Exim] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Exim] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Exim] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Exim] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Exim] SET ARITHABORT OFF 
GO
ALTER DATABASE [Exim] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Exim] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Exim] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Exim] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Exim] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Exim] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Exim] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Exim] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Exim] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Exim] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Exim] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Exim] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Exim] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Exim] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Exim] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Exim] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Exim] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Exim] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Exim] SET  MULTI_USER 
GO
ALTER DATABASE [Exim] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Exim] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Exim] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Exim] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Exim] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Exim] SET QUERY_STORE = OFF
GO
USE [Exim]
GO
/****** Object:  Table [dbo].[Currencies]    Script Date: 1/21/2021 2:31:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Currencies](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Currency] [varchar](5) NOT NULL,
 CONSTRAINT [PK_Currencies] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ExchangeRates]    Script Date: 1/21/2021 2:31:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExchangeRates](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Currency] [varchar](5) NOT NULL,
	[EffectiveDate] [date] NOT NULL,
	[ExchangeRate] [decimal](18, 0) NOT NULL,
 CONSTRAINT [PK_ExchangeRates] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PurchaseOrders]    Script Date: 1/21/2021 2:31:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PurchaseOrders](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PurchaseNo] [varchar](10) NOT NULL,
	[PurchaseDate] [date] NOT NULL,
	[PurchaseAmount] [decimal](18, 0) NOT NULL,
	[Currency] [varchar](5) NOT NULL,
	[SupplierName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_PurchaseOrders] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SalesOrders]    Script Date: 1/21/2021 2:31:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SalesOrders](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SalesNo] [varchar](20) NOT NULL,
	[SalesDate] [date] NOT NULL,
	[SalesAmount] [decimal](18, 0) NOT NULL,
	[Currency] [varchar](5) NOT NULL,
	[CustomerName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_SalesOrders] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Currencies] ON 

INSERT [dbo].[Currencies] ([ID], [Currency]) VALUES (1, N'USD')
SET IDENTITY_INSERT [dbo].[Currencies] OFF
SET IDENTITY_INSERT [dbo].[ExchangeRates] ON 

INSERT [dbo].[ExchangeRates] ([ID], [Currency], [EffectiveDate], [ExchangeRate]) VALUES (1, N'USD', CAST(N'2020-12-15' AS Date), CAST(14130 AS Decimal(18, 0)))
INSERT [dbo].[ExchangeRates] ([ID], [Currency], [EffectiveDate], [ExchangeRate]) VALUES (2, N'USD', CAST(N'2020-12-16' AS Date), CAST(14138 AS Decimal(18, 0)))
INSERT [dbo].[ExchangeRates] ([ID], [Currency], [EffectiveDate], [ExchangeRate]) VALUES (5, N'USD', CAST(N'2020-12-17' AS Date), CAST(14132 AS Decimal(18, 0)))
SET IDENTITY_INSERT [dbo].[ExchangeRates] OFF
SET IDENTITY_INSERT [dbo].[PurchaseOrders] ON 

INSERT [dbo].[PurchaseOrders] ([ID], [PurchaseNo], [PurchaseDate], [PurchaseAmount], [Currency], [SupplierName]) VALUES (1, N'POXX12002', CAST(N'2020-12-15' AS Date), CAST(10000 AS Decimal(18, 0)), N'USD', N'Hills and Sons')
INSERT [dbo].[PurchaseOrders] ([ID], [PurchaseNo], [PurchaseDate], [PurchaseAmount], [Currency], [SupplierName]) VALUES (2, N'POXX12003', CAST(N'2020-12-15' AS Date), CAST(14500 AS Decimal(18, 0)), N'USD', N'Gerlach-Huels')
INSERT [dbo].[PurchaseOrders] ([ID], [PurchaseNo], [PurchaseDate], [PurchaseAmount], [Currency], [SupplierName]) VALUES (3, N'POXX12004', CAST(N'2020-12-15' AS Date), CAST(5000 AS Decimal(18, 0)), N'USD', N'Berge Inc')
INSERT [dbo].[PurchaseOrders] ([ID], [PurchaseNo], [PurchaseDate], [PurchaseAmount], [Currency], [SupplierName]) VALUES (4, N'POXX12005', CAST(N'2020-12-15' AS Date), CAST(22000 AS Decimal(18, 0)), N'USD', N'Tremblay LLC')
INSERT [dbo].[PurchaseOrders] ([ID], [PurchaseNo], [PurchaseDate], [PurchaseAmount], [Currency], [SupplierName]) VALUES (5, N'POXX12006', CAST(N'2020-12-15' AS Date), CAST(12500 AS Decimal(18, 0)), N'USD', N'Kshlerin-Hansen')
INSERT [dbo].[PurchaseOrders] ([ID], [PurchaseNo], [PurchaseDate], [PurchaseAmount], [Currency], [SupplierName]) VALUES (6, N'POXX12007', CAST(N'2020-12-15' AS Date), CAST(7500 AS Decimal(18, 0)), N'USD', N'Auer, West and Mann')
INSERT [dbo].[PurchaseOrders] ([ID], [PurchaseNo], [PurchaseDate], [PurchaseAmount], [Currency], [SupplierName]) VALUES (7, N'POXX12008', CAST(N'2020-12-16' AS Date), CAST(10000 AS Decimal(18, 0)), N'USD', N'Stanton PLC')
INSERT [dbo].[PurchaseOrders] ([ID], [PurchaseNo], [PurchaseDate], [PurchaseAmount], [Currency], [SupplierName]) VALUES (8, N'POXX12009', CAST(N'2020-12-16' AS Date), CAST(29500 AS Decimal(18, 0)), N'USD', N'Gleason-Goyette')
INSERT [dbo].[PurchaseOrders] ([ID], [PurchaseNo], [PurchaseDate], [PurchaseAmount], [Currency], [SupplierName]) VALUES (9, N'POXX12010', CAST(N'2020-12-16' AS Date), CAST(11000 AS Decimal(18, 0)), N'USD', N'Gerlach-Huels')
INSERT [dbo].[PurchaseOrders] ([ID], [PurchaseNo], [PurchaseDate], [PurchaseAmount], [Currency], [SupplierName]) VALUES (10, N'POXX12011', CAST(N'2020-12-16' AS Date), CAST(20000 AS Decimal(18, 0)), N'USD', N'Berge Inc')
SET IDENTITY_INSERT [dbo].[PurchaseOrders] OFF
SET IDENTITY_INSERT [dbo].[SalesOrders] ON 

INSERT [dbo].[SalesOrders] ([ID], [SalesNo], [SalesDate], [SalesAmount], [Currency], [CustomerName]) VALUES (2, N'SOXX12002', CAST(N'2020-12-15' AS Date), CAST(25000 AS Decimal(18, 0)), N'USD', N'Jenkins, Walsh and Corkery')
INSERT [dbo].[SalesOrders] ([ID], [SalesNo], [SalesDate], [SalesAmount], [Currency], [CustomerName]) VALUES (3, N'SOXX12003', CAST(N'2020-12-15' AS Date), CAST(34500 AS Decimal(18, 0)), N'USD', N'Miller Inc')
INSERT [dbo].[SalesOrders] ([ID], [SalesNo], [SalesDate], [SalesAmount], [Currency], [CustomerName]) VALUES (4, N'SOXX12004', CAST(N'2020-12-15' AS Date), CAST(12000 AS Decimal(18, 0)), N'USD', N'Schaden PLC')
INSERT [dbo].[SalesOrders] ([ID], [SalesNo], [SalesDate], [SalesAmount], [Currency], [CustomerName]) VALUES (5, N'SOXX12005', CAST(N'2020-12-15' AS Date), CAST(50000 AS Decimal(18, 0)), N'USD', N'Girard Michaud SA')
INSERT [dbo].[SalesOrders] ([ID], [SalesNo], [SalesDate], [SalesAmount], [Currency], [CustomerName]) VALUES (6, N'SOXX12006', CAST(N'2020-12-15' AS Date), CAST(30000 AS Decimal(18, 0)), N'USD', N'Allard Perron SAS')
INSERT [dbo].[SalesOrders] ([ID], [SalesNo], [SalesDate], [SalesAmount], [Currency], [CustomerName]) VALUES (7, N'SOXX12007', CAST(N'2020-12-16' AS Date), CAST(17500 AS Decimal(18, 0)), N'USD', N'Tremblay Inc')
INSERT [dbo].[SalesOrders] ([ID], [SalesNo], [SalesDate], [SalesAmount], [Currency], [CustomerName]) VALUES (8, N'SOXX12008', CAST(N'2020-12-16' AS Date), CAST(22000 AS Decimal(18, 0)), N'USD', N'Heidenreich-Wisoky')
INSERT [dbo].[SalesOrders] ([ID], [SalesNo], [SalesDate], [SalesAmount], [Currency], [CustomerName]) VALUES (9, N'SOXX12009', CAST(N'2020-12-16' AS Date), CAST(66000 AS Decimal(18, 0)), N'USD', N'Pouros-Wuckert')
INSERT [dbo].[SalesOrders] ([ID], [SalesNo], [SalesDate], [SalesAmount], [Currency], [CustomerName]) VALUES (10, N'SOXX12010', CAST(N'2020-12-16' AS Date), CAST(29500 AS Decimal(18, 0)), N'USD', N'Corkery LLC')
INSERT [dbo].[SalesOrders] ([ID], [SalesNo], [SalesDate], [SalesAmount], [Currency], [CustomerName]) VALUES (11, N'SOXX12011', CAST(N'2020-12-17' AS Date), CAST(44000 AS Decimal(18, 0)), N'USD', N'Erdman Group')
INSERT [dbo].[SalesOrders] ([ID], [SalesNo], [SalesDate], [SalesAmount], [Currency], [CustomerName]) VALUES (13, N'SOXX12012', CAST(N'2020-12-17' AS Date), CAST(16500 AS Decimal(18, 0)), N'USD', N'Bernhard Inc')
INSERT [dbo].[SalesOrders] ([ID], [SalesNo], [SalesDate], [SalesAmount], [Currency], [CustomerName]) VALUES (15, N'SOXX12013', CAST(N'2020-12-17' AS Date), CAST(42500 AS Decimal(18, 0)), N'USD', N'Enhanced Homogeneous Groupware')
SET IDENTITY_INSERT [dbo].[SalesOrders] OFF
SET ANSI_PADDING ON
GO
/****** Object:  Index [UIX_Currencies]    Script Date: 1/21/2021 2:31:50 PM ******/
ALTER TABLE [dbo].[Currencies] ADD  CONSTRAINT [UIX_Currencies] UNIQUE NONCLUSTERED 
(
	[Currency] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UIX_ExchangeRates]    Script Date: 1/21/2021 2:31:50 PM ******/
ALTER TABLE [dbo].[ExchangeRates] ADD  CONSTRAINT [UIX_ExchangeRates] UNIQUE NONCLUSTERED 
(
	[Currency] ASC,
	[EffectiveDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UIX_PurchaseOrders]    Script Date: 1/21/2021 2:31:50 PM ******/
ALTER TABLE [dbo].[PurchaseOrders] ADD  CONSTRAINT [UIX_PurchaseOrders] UNIQUE NONCLUSTERED 
(
	[PurchaseNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UIX_SalesOrders]    Script Date: 1/21/2021 2:31:50 PM ******/
ALTER TABLE [dbo].[SalesOrders] ADD  CONSTRAINT [UIX_SalesOrders] UNIQUE NONCLUSTERED 
(
	[SalesNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ExchangeRates]  WITH CHECK ADD  CONSTRAINT [FK_ExchangeRates_Currencies] FOREIGN KEY([Currency])
REFERENCES [dbo].[Currencies] ([Currency])
GO
ALTER TABLE [dbo].[ExchangeRates] CHECK CONSTRAINT [FK_ExchangeRates_Currencies]
GO
ALTER TABLE [dbo].[PurchaseOrders]  WITH CHECK ADD  CONSTRAINT [FK_PurchaseOrders_Currencies] FOREIGN KEY([Currency])
REFERENCES [dbo].[Currencies] ([Currency])
GO
ALTER TABLE [dbo].[PurchaseOrders] CHECK CONSTRAINT [FK_PurchaseOrders_Currencies]
GO
ALTER TABLE [dbo].[SalesOrders]  WITH CHECK ADD  CONSTRAINT [FK_SalesOrders_Currencies] FOREIGN KEY([Currency])
REFERENCES [dbo].[Currencies] ([Currency])
GO
ALTER TABLE [dbo].[SalesOrders] CHECK CONSTRAINT [FK_SalesOrders_Currencies]
GO
/****** Object:  StoredProcedure [dbo].[P_GetActiveExchangeRates]    Script Date: 1/21/2021 2:31:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[P_GetActiveExchangeRates]
AS
BEGIN
	; WITH Cte AS (
		SELECT	*,
				ROW_NUMBER() OVER (PARTITION BY Currency ORDER BY EffectiveDate DESC) RowNo
		FROM	ExchangeRates
		WHERE	EffectiveDate <= CAST(GETDATE() AS DATE)
	)

	SELECT	Currency,
			EffectiveDate [Effective Date],
			ExchangeRate [Exchange Rate]
	FROM	Cte
	WHERE	RowNo = 1


END
GO
/****** Object:  StoredProcedure [dbo].[P_GetReport_PO]    Script Date: 1/21/2021 2:31:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[P_GetReport_PO]
AS
BEGIN
	; WITH Cte AS (
		SELECT *, ROW_NUMBER() OVER (PARTITION BY Currency ORDER BY EffectiveDate DESC) RowNo
		FROM ExchangeRates
	)

	SELECT	po.PurchaseNo [Purchase No],
			po.PurchaseDate [Purchase Date],
			po.PurchaseAmount [Purchase Amount],
			po.Currency,
			po.PurchaseAmount * c.ExchangeRate [Purchase Amount IDR],
			po.SupplierName [Supplier]
	FROM	PurchaseOrders po
			INNER JOIN  Cte c on c.Currency = po.Currency
				AND c.RowNo = 1

END
GO
/****** Object:  StoredProcedure [dbo].[P_GetReport_SO]    Script Date: 1/21/2021 2:31:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[P_GetReport_SO]
AS
BEGIN
	; WITH Cte AS (
		SELECT	*,
				ROW_NUMBER() OVER (PARTITION BY Currency ORDER BY EffectiveDate DESC) RowNo
		FROM	ExchangeRates
		WHERE	EffectiveDate <= CAST(GETDATE() AS DATE)
	)

	SELECT	so.SalesNo [Sales No],
			so.SalesDate [Sales Date],
			so.SalesAmount [Sales Amount],
			so.Currency,
			so.SalesAmount * c.ExchangeRate [Sales Amount IDR],
			so.CustomerName [Customer]
	FROM	SalesOrders so
			INNER JOIN  Cte c on c.Currency = so.Currency
				AND c.RowNo = 1

END
GO
USE [master]
GO
ALTER DATABASE [Exim] SET  READ_WRITE 
GO
